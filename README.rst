django-mongobrowser
===================

Mongobrowser is a simple Web-based browser for quick and easy browsing of your Mongo database from your Django app. It's also very much work in process.

It's not documented yet so you propably shouldn't use it.

Requirements
------------

* django-smartpaginator ( https://bitbucket.org/Uninen/django-smartpaginator )
* Django >= 1.3

Using Mongobrowser
------------------

1. Add `mongobrowser` to your `INSTALLED_APPS` and configure `MONGOBROWSER_HOST` and `MONGOBROWSER_PORT` in your settings.
2. Add `url(r'^mongobrowser/', include('mongobrowser.urls')),` to your `URLConf`.
3. Profit!11
