Ideas for future
----------------

- Create mongobrowser-meta collection for mb-related metadata
    - Cache collection sort keys here
    - implement automatically refreshing "firehose" view with predefined timestamp key (or 'timestamp)
- implement search
