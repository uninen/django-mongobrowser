#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from os.path import dirname, abspath
from optparse import OptionParser

from django.conf import settings

if not settings.configured:
    settings.configure(
        DATABASE_ENGINE='sqlite3',
        # HACK: this fixes our threaded runserver remote tests
        # DATABASE_NAME='test_sentry',
        # TEST_DATABASE_NAME='test_sentry',
        INSTALLED_APPS=[
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.sites',
            
            'django_nose',
            'mongobrowser',
            
            'tests',
        ],
        ROOT_URLCONF='',
        DEBUG=False,
        SITE_ID=1,
        TEST_RUNNER = 'django_nose.NoseTestSuiteRunner',
        INTERNAL_IPS = ('127.0.0.1',),
        MONGOBROWSER_ALLOW_EDITS = True,
        NOSE_ARGS = ( 
            '--with-xcover', 
            '--cover-package=mongobrowser', 
            '--cover-tests', 
            '--nocapture', 
            # '--testmatch=^test',
            '--exclude=mongobrowser$',
        )
        TEMPLATE_CONTEXT_PROCESSORS = (
            "django.contrib.auth.context_processors.auth",
            "django.core.context_processors.debug",
            "django.core.context_processors.i18n",
            "django.core.context_processors.media",
            "django.core.context_processors.static",
            "django.contrib.messages.context_processors.messages"
            "django.core.context_processors.request",
        )
    )
from django.core.management import call_command

def runtests():
    failures = call_command('test')
    sys.exit(failures)

if __name__ == '__main__':
    runtests()
