# -*- coding: utf-8 -*-
from contextlib import contextmanager

from django.conf import settings
from django.test import TestCase
from django.utils.unittest import skip
from django.core.urlresolvers import reverse
from django.test.client import RequestFactory

from pymongo import Connection

from mongobrowser.settings import MONGOBROWSER_HOST, MONGOBROWSER_PORT
from mongobrowser.utils import allow_edits

class SettingDoesNotExist:
    pass

@contextmanager
def patch_settings(**kwargs):
    from django.conf import settings
    old_settings = []
    for key, new_value in kwargs.items():
        old_value = getattr(settings, key, SettingDoesNotExist)
        old_settings.append((key, old_value))
        setattr(settings, key, new_value)
    yield
    for key, old_value in old_settings:
        if old_value is SettingDoesNotExist:
            delattr(settings, key)
        else:
            setattr(settings, key, old_value)


class UrlTestCase(TestCase):
    # TODO: Need to create and destroy an empty db for these tests!
    
    urls = 'tests.urls'

    def test_homepage_view(self):
        response = self.client.get(reverse('mongobrowser_index'))
        self.assertEqual(response.status_code, 200)

    def test_db_index_view(self):
        url = reverse('mongobrowser_database', kwargs={
            'database': 'test', 
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
    
    def test_collection_index_view(self):
        url = reverse('mongobrowser_collection', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_document_detail_view(self):
        url = reverse('mongobrowser_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
            'document_id': 'foo-id',
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
    
    def test_doc_creation(self):
        connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
        db = connection['test']
        collection = db['testcollection']

        hello_doc = { 
            "_id" : "4e14d84b95d994efbb25b20a", 
            "message" : "hello mongo!"
        }
        doc_id = collection.insert(hello_doc)
        
        # test doc url
        url = reverse('mongobrowser_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
            'document_id': '4e14d84b95d994efbb25b20a',
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # test doc raw url
        url = reverse('mongobrowser_raw_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
            'document_id': '4e14d84b95d994efbb25b20a',
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_doc_creation_2(self):
        connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
        db = connection['test']
        collection = db['testcollection']

        hello_doc = { 
            "_id" : "8285c3bc-bc66-4ad6-addc-9a44acf37e2a", 
            "message" : "hello mongo with non-standard pk!"
        }

        doc_id = collection.insert(hello_doc)

        url = reverse('mongobrowser_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
            'document_id': doc_id,
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
    
    def test_doc_deletion(self):

        url = reverse('mongobrowser_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
            'document_id': '4e14d84b95d994efbb25b20a',
        })

        expected_url = reverse('mongobrowser_collection', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
        })

        response = self.client.post(url, {
            'delete': True,
        })
        self.assertRedirects(response, expected_url, status_code=302, \
            target_status_code=200)
    
    def test_sorting(self):
        url = reverse('mongobrowser_collection', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
        })
        response = self.client.get(url + '?page=1&sort=_id&order=asc')
        self.assertEqual(response.status_code, 200)
        
        # wrong sort arguments should throw 404
        response = self.client.get(url + '?page=1&sort=_id&order=FOO')
        self.assertEqual(response.status_code, 404)

        response = self.client.get(url + '?page=1&sort=FOO&order=asc')
        self.assertEqual(response.status_code, 404)
    
    def test_editing(self):
        url = reverse('mongobrowser_edit_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
            'document_id': '4e162cd15faedb29b4000000',
        })
        
        hello_doc = u"{u'Message': u'Hello, Mongo! Again!', u'_id': ObjectId('4e162cd15faedb29b4000000')}"

        settings.DEBUG = False
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        settings.DEBUG = True
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, {
            'edit': True,
            'document': hello_doc,
        })
        self.assertEqual(response.status_code, 302)        

    
    def test_inserting(self):
        url = reverse('mongobrowser_insert_document', kwargs={
            'database': 'test', 
            'collection': 'testcollection',
        })
        hello_doc = u'{"Message" : "Hello, Mongo!"}'

        settings.DEBUG = False
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        settings.DEBUG = True
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, {
            'insert': True,
            'document': hello_doc,
        })
        self.assertEqual(response.status_code, 302)        
    
class FactoryTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_allow_edits(self):
        settings.DEBUG = True
        request = self.factory.get('/url')
        self.assertEqual(allow_edits(request), True)

    def test_allow_edits2(self):
        settings.DEBUG = True
        request = self.factory.get('/url')
        request.META['HTTP_X_FORWARDED_FOR'] = '127.0.0.2,10.10.10.10'
        self.assertEqual(allow_edits(request), False)

    @skip("Skipping until Django 1.4")
    def test_allow_edits3(self):
        settings.DEBUG = True
        settings.MONGOBROWSER_ALLOW_EDITS = False
        request = self.factory.get('/url')
        self.assertEqual(allow_edits(request), False)
