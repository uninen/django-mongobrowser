# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('mongobrowser.views',
    url(r'^$', 'index', name='mongobrowser_index'),
    url(r'^(?P<database>[-\w]+)/$', 'database', name='mongobrowser_database'),
    url(r'^(?P<database>[-\w]+)/(?P<collection>[-\w]+)/$', 'collection', name='mongobrowser_collection'),
    url(r'^(?P<database>[-\w]+)/(?P<collection>[-\w]+)/firehose/$', 'firehose', name='mongobrowser_firehose'),
    url(r'^(?P<database>[-\w]+)/(?P<collection>[-\w]+)/insert/$', 'insert_document', name='mongobrowser_insert_document'),
    url(r'^(?P<database>[-\w]+)/(?P<collection>[-\w]+)/(?P<document_id>[-\w]+)/$', 'document', name='mongobrowser_document'),
    url(r'^(?P<database>[-\w]+)/(?P<collection>[-\w]+)/(?P<document_id>[-\w]+)/raw/$', 'raw_document', name='mongobrowser_raw_document'),
    url(r'^(?P<database>[-\w]+)/(?P<collection>[-\w]+)/(?P<document_id>[-\w]+)/edit/$', 'edit_document', name='mongobrowser_edit_document'),
)