__all__ = ('VERSION',)
VERSION = (0, 1, 2)
__version__ = '.'.join(map(str, VERSION))
