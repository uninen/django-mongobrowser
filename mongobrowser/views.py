# -*- coding: utf-8 -*-
import json
from datetime import datetime, timedelta

from django.views.decorators.cache import never_cache
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import Http404, HttpResponse
from django.template import Context, loader
from django.views.decorators.csrf import csrf_exempt

from pymongo import Connection, ASCENDING, DESCENDING
from bson.objectid import ObjectId
from bson.errors import InvalidId
from bson.dbref import DBRef
ObjId = ObjectId # for edit eval()

from smartpaginator import get_pagination
from mongobrowser.settings import MONGOBROWSER_HOST, MONGOBROWSER_PORT
from mongobrowser.utils import allow_edits


def index(request):
    connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
    databases = connection.database_names()
    dbinfo = list()
    for database in databases:
        db = connection[database]
        stats = db.eval('db.stats();');
        dbinfo.append((database, stats))
    
    return render(request, 'mongobrowser/index.html', {
        'databases': dbinfo,
        'host': MONGOBROWSER_HOST,
    })

def database(request, database):
    connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
    db = connection[database]
    collections = db.collection_names()
    collections = [collection for collection in collections if collection != 'system.indexes']
    colinfo = list()
    for collection in collections:
        db = connection[database]
        stats = db.eval('db.%s.stats();' % collection);
        colinfo.append((collection, stats))

    return render(request, 'mongobrowser/database_index.html', {
        'db': database,
        'collections': colinfo,
        'host': MONGOBROWSER_HOST,
    })

def collection(request, database, collection):
    connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
    db = connection[database]
    collection_name = collection
    collection = db[collection]

    documents = collection.find()
    documents = list(documents)
    
    def unique_keys(items):
        seen = list()
        seen.append('_id')
        for item in items:
            for key in item.keys():
                if key not in seen and not key.startswith('_'):
                    seen.append(key)
                else:
                    pass
        return seen
    keys = unique_keys(documents)

    sort = request.GET.get('sort', '_id')
    if sort not in keys:
        raise Http404
    order = request.GET.get('order', 'desc')
    if order not in ('desc', 'asc'):
        raise Http404
        
    another_order = 'asc' if order == 'desc' else 'desc'
    sort_order = DESCENDING if order == 'desc' else ASCENDING
    documents = collection.find().sort(sort, sort_order)
    documents = get_pagination(request, documents)
    
    return render(request, 'mongobrowser/collection_index.html', {
        'db': database,
        'collection': collection_name,
        'documents': documents,
        'keys': keys,
        'sort': sort,
        'order': order,
        'another_order': another_order,
        'host': MONGOBROWSER_HOST,
        'allow_edits': allow_edits(request),
    })

@never_cache
@csrf_exempt
def firehose(request, database, collection):
    connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
    db = connection[database]
    collection_name = collection
    collection = db[collection]
    documents = collection.find(limit=25).sort("timestamp", -1)
    stats = db.eval('db.%s.stats();' % collection_name);

    now = datetime.now()
    hour_before = now - timedelta(hours=1)
    resent_docs = collection.find({"timestamp": {"$gt": hour_before }})
    docs_per_second = float(resent_docs.count() / 3600)

    context = {
        'db': database,
        'collection': collection_name,
        'documents': documents,
        'host': MONGOBROWSER_HOST,
        'stats': stats,
        'docs_per_second': docs_per_second,
    }

    if request.is_ajax():
        print "got AJAX request"
        template = loader.get_template('mongobrowser/firehose_table.html')
        context = Context(context)
        response = HttpResponse(mimetype='application/json')
        jquery_response = {
            "datatable": template.render(context),
            "second": docs_per_second
        }
        json_response = json.dumps(jquery_response)
        response.write(json_response)
        return response
    else:    
        return render(request, 'mongobrowser/firehose.html', context)
    
def _get_document(database, collection, document_id):
    connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
    db = connection[database]
    collection = db[collection]
    
    document = None

    document = collection.find_one({"_id": document_id})        
    if document is None:
        try:
            object_id = ObjectId(document_id)
            document = collection.find_one({"_id": object_id})
        except InvalidId:
            # import pdb; pdb.set_trace()
            raise Http404
    
    return document

def document(request, database, collection, document_id):

    document = _get_document(database, collection, document_id)

    if request.method == 'POST' and request.POST.has_key('delete'):
        connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
        db = connection[database]
        collection = db[collection]
        collection.remove({"_id": document['_id']}, safe=True)
        messages.add_message(request, messages.INFO, 'Deleted document %s.' % document['_id'])
        return redirect(reverse('mongobrowser_collection', kwargs={
            'database': database, 
            'collection': collection.name
            }, current_app='mongobrowser'))

    return render(request, 'mongobrowser/document_detail.html', {
        'db': database,
        'collection': collection,
        'document': document,
        'document_id': document_id,
        'host': MONGOBROWSER_HOST,
        'allow_edits': allow_edits(request),
    })

def raw_document(request, database, collection, document_id):
    document = _get_document(database, collection, document_id)
    response = HttpResponse(mimetype='text/json')
    response.write(document)
    return response

def edit_document(request, database, collection, document_id):
    if not allow_edits(request):
        raise Http404

    document = _get_document(database, collection, document_id)

    if request.method == 'POST' and request.POST.has_key('edit'):
        connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
        db = connection[database]
        collection = db[collection]
        doc_str = request.POST.get('document').strip()
        new_doc = eval(doc_str)
        collection.save(new_doc)
        messages.add_message(request, messages.INFO, 'Modifications saved for document %s.' % document['_id'])
        return redirect(reverse('mongobrowser_document', kwargs={
            'database': database, 
            'collection': collection.name,
            'document_id': document_id,
            }, current_app='mongobrowser'))

    return render(request, 'mongobrowser/document_edit.html', {
        'db': database,
        'collection': collection,
        'document': document,
        'document_id': document_id,
    })

def insert_document(request, database, collection):
    if not allow_edits(request):
        raise Http404

    if request.method == 'POST' and request.POST.has_key('insert'):
        connection = Connection(MONGOBROWSER_HOST, MONGOBROWSER_PORT)
        db = connection[database]
        collection = db[collection]
        doc_str = request.POST.get('document').strip()
        new_doc = eval(doc_str)
        collection.insert(new_doc)
        messages.add_message(request, messages.INFO, 'New document created.')
        return redirect(reverse('mongobrowser_collection', kwargs={
            'database': database, 
            'collection': collection.name
            }, current_app='mongobrowser'))

    return render(request, 'mongobrowser/document_create.html', {
        'db': database,
        'collection': collection,
        'document': document,
    })
