# -*- coding: utf-8 -*-
from django.conf import settings
from mongobrowser.settings import MONGOBROWSER_ALLOW_EDITS

def allow_edits(request):

    # edits allowed only if DEBUG=True
    if not getattr(settings, 'DEBUG', False):
        return False
    
    if not MONGOBROWSER_ALLOW_EDITS:
        return False
    
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
    if x_forwarded_for:
        remote_addr = x_forwarded_for.split(',')[0].strip()
    else:
        remote_addr = request.META.get('REMOTE_ADDR', None)
    
    if remote_addr not in settings.INTERNAL_IPS:
        return False
    
    return True
