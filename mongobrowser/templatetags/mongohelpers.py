# -*- coding: utf-8 -*-
from django import template

import mongobrowser

register = template.Library()

@register.filter(name='objectid')
def objectid(document):
    return document['_id']

@register.inclusion_tag('mongobrowser/templatetags/version.html')
def mongobrowser_version():
    return {'version': mongobrowser.__version__}
