# -*- coding: utf-8 -*-
from django.conf import settings

# Public
MONGOBROWSER_HOST = getattr(settings, 'MONGOBROWSER_HOST', 'localhost')
MONGOBROWSER_PORT = getattr(settings, 'MONGOBROWSER_PORT', 27017)
MONGOBROWSER_ALLOW_EDITS = getattr(settings, 'MONGOBROWSER_ALLOW_EDITS', True)
