#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import codecs
from setuptools import setup, find_packages

url = "https://bitbucket.org/Uninen/django-mongobrowser/"
if os.path.exists("README.rst"):
    long_description = codecs.open("README.rst", "r", "utf-8").read()
else:
    long_description = "See %s" % (url,)

setup(
    name='django-mongobrowser',
    version=__import__('mongobrowser').__version__,
    description="A Django app for browsing MongoDB databases.",
    long_description=long_description,
    author='Ville Säävuori',
    author_email='ville@unessa.net',
    license='BSD',
    url='https://bitbucket.org/Uninen/django-mongobrowser/',
    download_url='https://bitbucket.org/Uninen/django-mongobrowser/downloads/',
    packages=find_packages(exclude=('tests',)),
    tests_require=['django', 'pymongo', 'nose', 'django_nose', 'nosexcover'],
    test_suite='runtests.runtests',
    install_requires=['pymongo',],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
        'Topic :: Database :: Front-Ends',
    ],
)
